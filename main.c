#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CMD_NAME "--name"
#define SHORTCMD_NAME "-n"
#define CMD_HELP "--help"
#define SHORTCMD_HELP "-h"
#define CMD_DATE "--fecha"
#define SHORTCMD_DATE "-f"
#define CMD_DAY "--day"
#define SHORTCMD_DAY "-d"
#define CMD_YEAR "--year"
#define SHORTCMD_YEAR "-y"
#define CMD_MONTH "--month"
#define SHORTCMD_MONTH "-m"

#define MSG_HELP "Ayuda dell programa : utilize -f para indicar la fecha"
#define MSG_DATE "Cambio de fecha : "

typedef enum {ST_OK,ST_ERROR_FECHA,ST_ERROR_MES,ST_ERROR_DIA} status_cmd;


status_cmd leer_cmd(int cant_argumentos , char  * argumentos[]);


int main(int argc,char * argv[])
{
	
	

	leer_cmd(argc,argv);

	return EXIT_SUCCESS;
	
}


status_cmd leer_cmd(int cant_argumentos , char  * argumentos[])
{
	int i;

	for( i = 1; i < cant_argumentos ; i++ )
	{

		if( strcmp(argumentos[i],CMD_HELP) == 0  || strcmp(argumentos[i],SHORTCMD_HELP) == 0 )
			puts(MSG_HELP);
			

		else if (strcmp(argumentos[i],CMD_DATE) == 0  || strcmp(argumentos[i],SHORTCMD_DATE) == 0 )
			puts(MSG_DATE);


	}
	return ST_OK;
	
}


